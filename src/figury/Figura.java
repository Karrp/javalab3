package figury;

import java.awt.*;
import java.awt.geom.*;

public interface Figura
{
    public void rysuj(Graphics2D g2d);
    public void przesun(double x, double y, double predkosc);
    public boolean wewnatrz(double x, double y);
}
