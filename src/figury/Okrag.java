package figury;

import punkty.Punkt2D;

import java.awt.*;
import java.awt.geom.*;

public class Okrag implements Figura {
    Punkt2D p;
    double r;
    Ellipse2D circle;

    public Okrag(Punkt2D srodek, double promien)
    {
        p=srodek;
        r=promien;
    }

    public Okrag(double x, double y, double promien)
    {
        p = new Punkt2D(x, y);
        r = promien;
    }

    @Override
    public void rysuj(Graphics2D g2d)
    {
        circle = new Ellipse2D.Double(p.getX()-r ,p.getY()-r , r*2, r*2);
        //g2d.draw(circle); //obrys
        g2d.setColor(Color.BLUE); //zmień kolor
        g2d.fill(circle);
    }

    @Override
    public void przesun(double x, double y, double predkosc)
    {
        p.setX(p.getX()+predkosc*x);
        p.setY(p.getY()+predkosc*y);
    }

    @Override
    public boolean wewnatrz(double x, double y)
    {
        return circle.contains(x,y);
    }


}
