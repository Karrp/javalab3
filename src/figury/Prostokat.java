package figury;

import punkty.Punkt2D;

import java.awt.*;
import java.awt.geom.Rectangle2D;

public class Prostokat implements Figura
{
    Punkt2D p1, p2;
    Rectangle2D prostokat;

    public Prostokat(Punkt2D p1, Punkt2D p2)
    {
        this.p1 = p1;
        this.p2 = p2;
    }

    public Prostokat(Punkt2D p, double bok)
    {
        p1 = p;
        p2 = new Punkt2D(p.getX()+bok, p.getY()+bok);
    }

    public Prostokat(double x1, double y1, double bok)
    {
        p1 = new Punkt2D(x1, y1);
        p2 = new Punkt2D(x1+bok, y1+bok);
    }

    public Prostokat(Punkt2D p, double wysokosc, double szerokosc)
    {
        p1 = p;
        p2 = new Punkt2D(p.getX()+szerokosc, p.getY()+wysokosc);
    }

    public Prostokat(double x1, double y1, double x2, double y2)
    {
        p1 = new Punkt2D(x1, y1);
        p2 = new Punkt2D(x2, y2);
    }

    @Override
    public void rysuj(Graphics2D g2d)
    {
        prostokat = new Rectangle2D.Double(p1.getX(),p1.getY(), p2.getX()-p1.getX(),p2.getY()-p1.getY());
        g2d.setColor(Color.BLACK); //zmień kolor
        g2d.fill(prostokat);

    }

    @Override
    public void przesun(double x, double y, double predkosc)
    {
        p1.setX(p1.getX()+predkosc*x);
        p1.setY(p1.getY()+predkosc*y);

        p2.setX(p2.getX()+predkosc*x);
        p2.setY(p2.getY()+predkosc*y);
    }

    @Override
    public boolean wewnatrz(double x, double y)
    {
        boolean temp = prostokat.contains(x,y);
        return temp;
    }
}

