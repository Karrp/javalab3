package figury;

import punkty.Punkt2D;

import java.awt.*;
import java.awt.geom.Path2D;

public class Trojkat implements Figura {
    Punkt2D p1, p2, p3;
    Path2D path;

    public Trojkat(Punkt2D p1, Punkt2D p2, Punkt2D p3)
    {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }

    public Trojkat(double x1, double y1, double x2, double y2, double x3, double y3)
    {
        p1 = new Punkt2D(x1,y1);
        p2 = new Punkt2D(x2,y2);
        p3 = new Punkt2D(x3,y3);
    }

    @Override
    public void rysuj(Graphics2D g2d)
    {
        path = new Path2D.Double();
        path.moveTo(p1.getX(), p1.getY());
        path.lineTo(p2.getX(), p2.getY());
        path.lineTo(p3.getX(), p3.getY());
        path.closePath();

        //g2d.draw(path);//obrys
        g2d.setColor(Color.GREEN); //zmień kolor
        g2d.fill(path);
    }

    @Override
    public void przesun(double x, double y, double predkosc)
    {
        p1.setX(p1.getX()+predkosc*x);
        p1.setY(p1.getY()+predkosc*y);

        p2.setX(p2.getX()+predkosc*x);
        p2.setY(p2.getY()+predkosc*y);

        p3.setX(p3.getX()+predkosc*x);
        p3.setY(p3.getY()+predkosc*y);

    }

    @Override
    public boolean wewnatrz(double x, double y)
    {
        return path.contains(x,y);
    }


}
