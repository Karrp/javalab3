package punkty;

import java.util.Comparator;

public class PktPor implements Comparator <Punkt1D>
{
    @Override
    public int compare (Punkt1D p1, Punkt1D p2)
    {
        double r1 = p1.odSrodka();
        double r2 = p2.odSrodka();

        if (r1 > r2)
            return 1;
        else if (r1 == r2)
            return 0;
        else
            return -1;
    }
}
