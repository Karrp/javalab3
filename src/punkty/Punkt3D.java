package punkty;

public class Punkt3D extends Punkt2D
{
    double z;

    public Punkt3D()
    {
        super();
        z=0;
    }

    public Punkt3D(double x)
    {
        super(x);
        z=0;
    }

    public Punkt3D(double x, double y)
    {
        super(x, y);
        z=0;
    }

    public Punkt3D(double x, double y, double z)
    {
        super(x, y);
        this.z=z;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z)
    {
        this.z = z;
    }

    public double odSrodka()
    {
        return Math.sqrt( Math.pow(x,2) + Math.pow(y, 2) + Math.pow(z,2) );
    }

    @Override
    public String toString()
    {
        return super.toString()+" "+z;
    }
}
