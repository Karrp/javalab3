package punkty;

//import java.util.Random;

public class Punkt1D
{
    double x;

    public Punkt1D()
    {
        //Random rand = new Random();
        //x=rand.nextDouble();
        x=0;
    }

    public Punkt1D(double x)
    {
        this.x=x;
    }

    public double getX() {
        return x;
    }

    public void setX(double x)
    {
        this.x = x;
    }

    public double odSrodka()
    {
        return x;
    }

    @Override
    public String toString()
    {
        return String.valueOf(x);
    }
}