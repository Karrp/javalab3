package punkty;

public  class Punkt2D extends Punkt1D
{
    double y;

    public Punkt2D()
    {
        super();
        y=0;
    }

    public  Punkt2D(double x)
    {
        super(x);
    }

    public Punkt2D(double x, double y)
    {
        super(x);
        this.y=y;
    }

    public double getY() {
        return y;
    }

    public void setY(double y)
    {
        this.y = y;
    }

    public double odSrodka()
    {
        return Math.sqrt( Math.pow(x,2) + Math.pow(y, 2) );
    }

    @Override
    public String toString()
    {
        return super.toString()+" "+y;
    }
}
