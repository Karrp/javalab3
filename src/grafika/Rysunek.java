package grafika;

import figury.Figura;
import figury.Okrag;
import figury.Prostokat;
import figury.Trojkat;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public class Rysunek extends JPanel implements KeyListener, MouseListener
{
    private ArrayList<Figura> lista;

    public Rysunek( ArrayList<Figura> lista)
    {
        this.lista=lista;
        addKeyListener(this);
        addMouseListener(this);
        setFocusable(true);

        //zmień figury ilość i kształty
        lista.add(new Okrag(0,0,100));
        for(int i=0; i<3; ++i)
            for (int j=0; j<3; ++j)
            {
                lista.add(new Okrag(100+i*40,200+j*40,10));
            }
        lista.add(new Trojkat(10,10,20,100,150,70));
        lista.add(new Trojkat(300,100,250,400,400,300));
        lista.add(new Prostokat(100, 100, 300, 150));
        lista.add(new Prostokat(250,300,50));

        //rozmiar okna
        setPreferredSize(new Dimension(400,400)); //zmień

    }

    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        //fajna stronka
        //https://www.ntu.edu.sg/home/ehchua/programming/java/J8b_Game_2DGraphics.html



        for (int i=0; i<lista.size(); ++i)
        {
            lista.get(i).rysuj(g2d);
        }

        //t.start();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e)
    {
        switch(e.getKeyCode())
        {
            case KeyEvent.VK_UP:
            {
                lista.get(lista.size()-1).przesun(0,-1,5);
                break;
            }
            case KeyEvent.VK_DOWN:
            {
                lista.get(lista.size()-1).przesun(0,1,5);
                break;
            }
            case KeyEvent.VK_LEFT:
            {
                lista.get(lista.size()-1).przesun(-1,0,5);
                break;
            }
            case KeyEvent.VK_RIGHT:
            {
                lista.get(lista.size()-1).przesun(1,0,5);
                break;
            }
            default:
            {
                break;
            }
        }
        repaint();
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
        int i=lista.size()-1;
        double x=e.getX(), y=e.getY();
        do {
            boolean bul = lista.get(i).wewnatrz(x,y);
            if(bul)
            {
                Figura temp = lista.remove(i);
                lista.add(temp);
                i=0;
            }
            --i;
        }while (i>=0 && !lista.get(i+1).wewnatrz(x,y) );
        repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
