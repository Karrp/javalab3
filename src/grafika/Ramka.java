package grafika;

import figury.Figura;
import figury.Okrag;
import figury.Prostokat;
import figury.Trojkat;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

public class Ramka extends JFrame
{
    ArrayList<Figura> lista = new ArrayList<>();
    public Ramka()
    {
        //---parametry okna
        super("Rysuj"); //zmień

        JPanel panel = new Rysunek(lista);
        add(panel);


        //
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        //setResizable(true);//domyślnie true
        setLocation(500,300); //zmień jeżeli okno pojawia Ci się poza ekranem
        //setSize(320,180);
        pack();
    }

}
